package com.example.android.modul5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class listartikel extends AppCompatActivity {

    RecyclerView recyclerView;
    artikeladapter articleAdapter;
    ArrayList<artikel> articleList;
    artikeldb dbHelper;

    final String PREF_NIGHT_MODE = "NightMode";
    SharedPreferences spNight;
    private Object listartikel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        }else{
            setTheme(R.style.AppTheme);

            spNight = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);
            if(spNight.getBoolean(PREF_NIGHT_MODE,false)){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_article);

        dbHelper = new artikeldb(this);
        recyclerView = findViewById(R.id.rv_article);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        articleList = new ArrayList<artikel>();

        dbHelper.readData(articleList);

        articleAdapter = new artikeladapter(this, articleList);
        recyclerView.setAdapter(articleAdapter);

        articleAdapter.notifyDataSetChanged();
    }
}
